# Building MuPDF library for using Android NDK (linux) #

### Overview ###
MuPDF is a useful open source library for adding Pdf viewing. It uses native C/C++ code, which is why it’s necessary to build the library in Android NDK, before integrating it in Android app.

The MuPDF site contains downloads and documentations on the library, as well as a concise build instructions.

In this document we’ll outline a more detail step by step guide to build the library in linux (Ubuntu 14.04).

### Note 
This document doesn’t discuss integrating the generated library in an Android app, only the build process, and running the sample app.

We’ll refer to the instructions in ReadMe.txt file available in the source folder in Step 1 below (i.e. <source_folder>/platform/android/viewer/ReadMe.txt)

### Prerequisite ###
JDK, and Android SDK, and NDK must be installed in the system (at least API 16).
 
[NDK installation guide](https://developer.android.com/ndk/guides/index.html)

### Start ###

**a) Get the MuPDF source code**

Check out a copy of the mupdf android viewer source from git:

```
#!python

git clone --recursive git://git.ghostscript.com/mupdf-android-viewer-old.git
```


**b) Set SDK and NDK path in local.properties file**

It’s in 
**<source_folder>/platform/android/viewer/local.properties**

(You can keep a copy of the file for e.g. local.properties.sample)

Then change the below 2 lines

```
#!python


sdk.dir=/path/to/android/sdk
ndk.dir=/path/to/android/ndk
```


**c) Adding NDK to PATH**

Create an ant.sh file at **/etc/profile.d** folder, in enter following lines:


```
#!python

export ANDROID_NDK_HOME=/home/raqib/Documents/Android/sdk/ndk-bundle
export PATH=${ANDROID_NDK_HOME}:${PATH}
```

**d) Installing ANT (Several ways mentioned here)**

We’ll use 

```
#!python

apt-get install ant
```


**e) Adding ANT_HOME and ANT_HOME/bin to PATH**

For e.g. my Ant location is  /usr/share/ant

Create an **ant.sh** file at **/etc/profile.d** folder

Enter the following content to the file:


```
#!python

export ANT_HOME=/usr/share/ant
export PATH=${ANT_HOME}/bin:${PATH}
```

**f) Activate above environment variables (Step c & e)**

We can do that by log out and log in again or simply run below command:

```
#!python

source /etc/profile
```


We can verify whether Maven is installed successfully or not by type command:

```
#!python

ant -version
```


The result will be similar to this:

```
#!python

Apache Ant(TM) version 1.9.3 compiled on April 8 2014
```


(Note: There are several ways to set environment variables as mentioned here. Files with the .sh extension in the **/etc/profile.d** directory get executed whenever a bash login shell is entered (e.g. when logging in from the console or over ssh), as well as by the DisplayManager when the desktop session loads)

**g) Prepare the auto-generated files** - a copy of a 'generated' directory (not available in download)

According to the readme file, this step is very crucial, and often skipped.
On linux/macos, give command 

```
#!python

make generate
```
 
in the top level directory of the downloaded source code.

**h) Build the native code libraries**
Change into the platform/android/viewer directory (note, the platform/android/viewer directory, NOT the platform/android/viewer/jni directory!), and execute:
       	
```
#!python

ndk-build
```

This should build the native code portion.
If this dies with an error in thirdparty/jbig2/os_types.h load this
file into an editor, and change line 43 from:
    		
```
#!python

#else
```

to
    		
```
#!python

#elif !defined(HAVE_STDINT_H)
```
and this should solve the problem.

**i) The library *.so file is generated**

source/platform/android/viewer/libs/armeabi-v7a/libmupdf_java32.so

**j) Build the java application**

Execute 
		
```
#!c

ant debug
```

**k) Running the demo app**

If build is successful, the demo Apk will be available in the bin folder

**platform/android/viewer/bin/MuPDF-debug.apk**
Which we can transfer to the device, install and run.
The ReadMe file also include steps to install and run in emulator.